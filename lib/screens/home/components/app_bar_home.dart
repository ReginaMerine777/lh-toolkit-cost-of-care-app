import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../widgets/user_location.dart';

class AppBarHome extends StatelessWidget {
  final GlobalKey<ScaffoldState> drawerKey;

  AppBarHome(this.drawerKey);

  @override
  Widget build(BuildContext context) {
    const Color appBackgroundColor = Color(0xFFFFF7EC);

    return Container(
      height: 90,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, bottom: 4, right: 20, top: 35),
        decoration: BoxDecoration(
            color: Colors.grey[50],
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2.0,
              ),
            ],
            border: Border.all(
              color: Colors.grey[100],
            ),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Material(
          child: InkWell(
            onTap: () => {Navigator.pushNamed(context, '/SearchProcedure')},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  onPressed: () {
                    drawerKey.currentState.openDrawer();
                  },
                  icon: Icon(
                    Icons.menu,
                    color: Colors.black,
                    size: 25,
                  ),
                ),
                Expanded(
                    child: Text(
                  'Search for Procedures, Pharmacy, DRG',
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 16,
                  ),
                  overflow: TextOverflow.ellipsis,
                )),
                IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: Colors.black,
                    size: 25,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/SettingsHome',
                        arguments: UserLocation(appBackgroundColor));
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
