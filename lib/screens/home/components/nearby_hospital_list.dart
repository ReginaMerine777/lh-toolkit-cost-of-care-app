import 'package:cost_of_care/bloc/location_bloc/location_bloc.dart';
import 'package:cost_of_care/bloc/location_bloc/user_location_state.dart';
import 'package:cost_of_care/bloc/nearby_hospital_bloc/bloc.dart';
import 'package:cost_of_care/models/hospitals.dart';
import 'package:cost_of_care/screens/home/components/list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cost_of_care/main.dart';
import 'package:cost_of_care/bloc/location_bloc/user_location_events.dart';

class NearbyHospitalList extends StatelessWidget {
  static const Color appBackgroundColor = Color(0xFFFFF7EC);
  final userLocationWidget;

  const NearbyHospitalList(this.userLocationWidget);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LocationBloc, LocationState>(
      listener: (BuildContext context, state) {
        if (state is LocationLoaded) {
          String state = box.get('state');
          context.read<NearbyHospitalBloc>().add(FetchHospitals(state, 1));

          //Compare hospital bloc
        } else if (state is LocationLoading) {
          context.read<NearbyHospitalBloc>().add(LoadEvent());
        } else if (state is LocationError) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
              state.message,
              style: TextStyle(color: Colors.white),
            ),
            duration: Duration(seconds: 6),
            action: SnackBarAction(
              label: "Retry",
              onPressed: () {
                BlocProvider.of<LocationBloc>(context).add(FetchLocation());
              },
            ),
          ));
          context
              .read<NearbyHospitalBloc>()
              .add(NearbyHospitalShowError(state.message));
        }
      },
      child: BlocBuilder<NearbyHospitalBloc, NearbyHospitalState>(
        builder: (BuildContext context, NearbyHospitalState state) {
          if (state is NearbyHospitalsLoadingState) {
            String state = box.get('state');
            if(state!=null)
            context.read<NearbyHospitalBloc>().add(FetchHospitals(state, 1));
            return shimmerLoading(userLocationWidget);
          } else if (state is NearbyHospitalsLoadedState) {
            return ListBuilder(
                state.nearbyHospital, userLocationWidget, state.page);
          } else if (state is NearbyHospitalsErrorState) {
            return Container(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 4),
                  Text(
                    state.message,
                    maxLines: 3,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),
                  ),
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      primary: Colors.white,
                      side: BorderSide(
                        color: Theme.of(context).primaryColor,
                        width: 1,
                      ),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                    ),
                    onPressed: () {
                      if (state.message ==
                              "You need to Enable GPS/Location Service" ||
                          state.message ==
                              "Permission Denied, Enable from Settings") {
                        BlocProvider.of<LocationBloc>(context)
                            .add(FetchLocation());
                      } else {

                        String stateName = box.get('state');
                        if(stateName!=null)
                        context
                            .read<NearbyHospitalBloc>()
                            .add(FetchHospitals(stateName, 1));
                      }
                    },
                    child: Text(
                      'RETRY',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }

          return Container();
        },
      ),
    );
  }
}

Widget shimmerLoading(userLocationWidget) {
  return ListView.builder(
    scrollDirection: Axis.vertical,
    physics: NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    itemCount: 7,
    itemBuilder: (BuildContext context, int index) {
      if (index == 0) return userLocationWidget;
      return Card(
        elevation: 4.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.grey[50]),
          child: makeShimmerListTile(context, index),
        ),
      );
    },
  );
}

class ListBuilder extends StatelessWidget {
  final List<Hospitals> items;
  final userLocationWidget;
  final int totalItems;
  ListBuilder(this.items, this.userLocationWidget, this.totalItems);

  @override
  Widget build(BuildContext context) {
    int count =
        items.length < totalItems + 1 ? items.length + 1 : totalItems + 1;
    return Scrollbar(
      child: ListView.builder(
        itemCount: count,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(0.0),
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) return userLocationWidget;
          return makeCard(items[index - 1]);
        },
      ),
    );
  }
}
