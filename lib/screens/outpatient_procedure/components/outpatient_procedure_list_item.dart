import 'package:flutter/material.dart';

import 'package:cost_of_care/models/outpatient_procedure.dart';

Card makeCard(OutpatientProcedure outpatientProcedure) {
  return Card(
    elevation: 4.0,
    margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
      decoration: BoxDecoration(color: Colors.grey[50]),
      child: makeListTile(outpatientProcedure),
    ),
  );
}

ListTile makeListTile(OutpatientProcedure outpatientProcedure) {
  return ListTile(
    title: Text(
      outpatientProcedure.name,
      style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
    ),
    trailing: Text(
      '\$ ' + outpatientProcedure.charge.toStringAsFixed(2),
      style: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
