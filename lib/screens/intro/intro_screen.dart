import 'package:cost_of_care/main.dart';
import 'package:flutter/material.dart';
import '../intro/intro_slider/intro_slider.dart';
import '../intro/intro_slider/slide_object.dart';

class IntroScreen extends StatefulWidget {
  IntroScreen({Key key}) : super(key: key);

  @override
  IntroScreenState createState() => new IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = [];
  int index = 0;

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "LibreHealth \n Cost Of Care Explorer \n By",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 36.0,
          fontWeight: FontWeight.bold,
        ),
        description:
            "We care for you by providing cost estimates for medical procedures of your nearby hospitals in US",
        styleDescription: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'Raleway'),
        pathImage: "assets/libre_white.png",
        colorBegin: Colors.orange,
        colorEnd: Colors.orange,
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
        maxLineTextDescription: 4,
      ),
    );
    slides.add(
      new Slide(
        title: "Compare Prices Of Medical Procedures",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
        ),
        description:
            "Either view CDM of a hospital or compare prices from lots of different hospitals, this App handles everything perfectly",
        styleDescription: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'RobotoMono'),
        pathImage: "assets/onboardingPage2.png",
      ),
    );
    slides.add(
      new Slide(
        title: "Compare Hospitals by Ratings & Patient Experience",
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
        ),
        description:
            "With medicare data, we provide functionality to compare hospitals by general information & patient reviews",
        styleDescription: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'RobotoMono'),
        pathImage: "assets/onboardingPage3.png",
      ),
    );
  }

  void onDonePress() {
    box.put('introDisplayed', true);
    Navigator.popAndPushNamed(context, '/BaseClass');
  }

//this will render the next button on the first page
  Widget renderNextBtn() {
    return Text(
      'Next',
      style: TextStyle(
        color: Colors.white,
      ),
    );
  }

// This will render the next button on all the pages excpet first
  Widget renderNextBtn2() {
    return Text(
      'Next',
      style: TextStyle(
        color: Colors.black,
      ),
    );
  }

  Widget renderDoneBtn() {
    return Text(
      'Let\'s Go',
      style: TextStyle(
        color: Colors.black,
      ),
    );
  }

//this will render the skip button on the first page
  Widget renderSkipBtn() {
    return Text(
      'Skip',
      style: TextStyle(
        color: Colors.white,
      ),
    );
  }

// This will render the skip button on all the pages excpet first
  Widget renderSkipBtn2() {
    return Text(
      'Skip',
      style: TextStyle(
        color: Colors.black,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // List slides
      slides: this.slides,

      // Skip button
      renderSkipBtn: this.renderSkipBtn(),
      renderSkipBtn2: this.renderSkipBtn2(),
      colorSkipBtn: Color(0x33000000),
      highlightColorSkipBtn: Color(0xff000000),

      // Next button
      renderNextBtn: this.renderNextBtn(),
      renderNextBtn2: this.renderNextBtn2(),

      // Done button
      renderDoneBtn: this.renderDoneBtn(),
      onDonePress: this.onDonePress,
      colorDoneBtn: Color(0x33000000),
      highlightColorDoneBtn: Color(0xff000000),

      // Dot indicator
      colorDot: Colors.black,
      colorActiveDot: Colors.grey,
      sizeDot: 13.0,

      // Show or hide status bar
      backgroundColorAllSlides: Colors.grey,

      //Tab
      listCustomTabs: renderTabs(),
    );
  }

  List<Widget> renderTabs() {
    var media = MediaQuery.of(context).size;
    List<Widget> tab = new List<Widget>();
    tab.add(Container(
      color: Colors.orange,
      height: double.infinity,
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: media.width * 0.0255),
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).padding.top + media.height * 0.057,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: media.height * 0.137),
              child: Text(
                slides[0].title,
                maxLines: 3,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: media.width * 0.09,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
                padding: EdgeInsets.only(bottom: media.height * 0.11),
                child: Image.asset("assets/libre_white.png")),
            Text(
              slides[0].description,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: media.width * 0.05,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'RobotoMono'),
            ),
          ],
        ),
      ),
    ));
    for (int i = 1; i < this.slides.length; i++) {
      tab.add(Container(
        color: Colors.orange,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: SizedBox(
                height: media.height * 0.11,
                child: Center(
                  child: Text(
                    slides[i].title,
                    style: slides[i].styleTitle,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(media.width * 0.1),
                        topRight: Radius.circular(media.width * 0.1))),
                child: Column(
                  children: [
                    Padding(
                        padding: EdgeInsets.only(
                          top: media.height * 0.028,
                          bottom: media.height * 0.114,
                          right: media.height * 0.028,
                          left: media.height * 0.028,
                        ),
                        child: Center(child: Image.asset(slides[i].pathImage))),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: media.height * 0.028),
                      child: Text(
                        slides[i].description,
                        style: slides[i].styleDescription,
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ));
    }
    return tab;
  }
}
